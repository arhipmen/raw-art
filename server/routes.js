const router = require("express").Router();
const nodemailer = require("nodemailer");
const db = require("./db.json");
const config = require("./config");

router.get("/pictures", (req, res) => {
	const response = db.pictures;
	res.json(response);
});

router.get("/pictures/:id", (req, res) => {
	const response = db.pictures.find(item => item.id == req.params.id);
	res.json(response);
});

router.get("/news", (req, res) => {
	const response = db.news;
	res.json(response);
});

router.get("/news/:id", (req, res) => {
	const response = db.news.find(item => item.id == req.params.id);
	res.json(response);
});

router.get("/question", (req, res) => {
	const response = db.question;
	res.json(response);
});

router.get("/question/:id", (req, res) => {
	const response = db.question.find(item => item.url == req.params.id);
	res.json(response);
});

router.post("/email", (req, res) => {
	let output = "";
	if (req.body.titlePic) {
		output = `
     <h2>Заказ на покупку картины: ${req.body.titlePic}</h2>
     <h3>Contact Details</h3>
     <ul>
       <li>Name: ${req.body.nameModal}</li>
       <li>Phone: ${req.body.phone}</li>
     </ul>
   `;
	} else {
		output = `
     <p>Это сообщение отпралено со страницы - "Контакты"</p>
     <h3>Contact Details</h3>
     <ul>
       <li>Name: ${req.body.name}</li>
       <li>Phone: ${req.body.phone}</li>
       <li>Email: ${req.body.email}</li>
     </ul>
     <h3>Message</h3>
     <p>${req.body.message}</p>
   `;
	}

	const transporter = nodemailer.createTransport(config.transporter);

	let mailOptions = {
		from: "elenagalaxy.art@yandex.ru",
		to: config.sendTo,
		subject: config.subject,
		text: req.body.content,
		html: output,
	};

	transporter.sendMail(mailOptions, (error, info) => {
		if (error) {
			arrResponse = {
				status: "error",
				error: error,
			};
			res.status(500);
		} else {
			arrResponse = {
				status: "success",
				data: info.accepted,
			};
			console.log(info.accepted);
			res.json({ status: "Ok", message: "Сообщение отправлено" });
		}
		res.end();
	});
});

module.exports = router;
