const express = require("express");
const bodyParser = require("body-parser");
const nodemailer = require("nodemailer");
const multer = require("multer");

const config = require("./config");
const mailer = express();
let arrResponse = {};

const upload = multer({ dest: "./uploads/" });

// Body Parser Middleware
mailer.use(bodyParser.urlencoded({ extended: false }));
mailer.use(bodyParser.json());

// Nodemailer POST
mailer.post("/email", upload.array("attachment", 2), (req, res) => {
  const output = `
    <p>You have a new contact request</p>
    <h3>Contact Details</h3>
    <ul>  
      <li>Name: ${req.body.name}</li>
      <li>Phone: ${req.body.phone}</li>
      <li>Email: ${req.body.email}</li>
    </ul>
    <h3>Message</h3>
    <p>${req.body.message}</p>
  `;

  const transporter = nodemailer.createTransport(config.transporter);

  let attachmentList = [];

  for (let i = 0; i < req.files.length; i++) {
    attachmentList.push({
      filename: req.files[i].originalname,
      path: req.files[i].path
    });
  }

  let mailOptions = {
    from: '"Nodemailer Contact" <' + config.transporter.auth.user + ">",
    to: config.sendTo,
    subject: config.subject,
    text: req.body.content,
    html: output,
    attachments: attachmentList
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      arrResponse = {
        status: "error",
        error: error
      };
      res.status(500);
    } else {
      arrResponse = {
        status: "success",
        data: info.accepted
      };
      console.log(info.accepted);
      res.redirect("/");
    }
    res.end();
  });
});

// GET notifications
mailer.get("/email", (req, res, next) => {
  res.send(arrResponse.status || null);
});

module.exports = mailer;
