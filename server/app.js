const express = require("express");
const bodyParser = require("body-parser");
// const authRoutes = require("./routes/auth.routes");
// const postRoutes = require("./routes/post.routes");
const app = express();
const router = require("./routes");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use("/api", router);

module.exports = app;
