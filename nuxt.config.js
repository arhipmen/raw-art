const colors = require("vuetify/es5/util/colors").default;

module.exports = {
  mode: "universal",
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: "%s - " + "ElenaGalaxy.Art",
    title: "ElenaGalaxy.Art",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      }
    ],
    // script: [
    //   {
    //     src: "/js/jquery-1.12.4.min.js",
    //     type: "text/javascript"
    //   }
    // ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff" },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ["@nuxtjs/vuetify"],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    [
      "@nuxtjs/yandex-metrika",
      {
        id: "56692549",
        webvisor: true,
        clickmap: true,
        useCDN: false,
        trackLinks: true,
        accurateTrackBounce: true
      }
    ],
    [
      "nuxt-i18n",
      {
        /* module options */
      }
    ]
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    host:
      process.env.NODE_ENV === "production" ? "elenagalaxy.art" : "localhost",
    port: process.env.NODE_ENV === "production" ? 443 : 3000,
    https: true
  },
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
          bgcontent: "#ababab",
          bgdrawer: "#242b2e",
          hover: "#25b1af",
          navbg: "#31393d"
        }
      }
    }
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    // transpile: ["vue-product-zoomer"],
    extend(config, ctx) {}
  },
  i18n: {
    locales: [
      {
        code: "en",
        iso: "en-US",
        name: "English",
        icon: "img/english.svg"
      },
      {
        code: "ru",
        iso: "ru-RU",
        name: "Русский",
        icon: "img/russia.svg"
      }
    ],
    vueI18nLoader: true,
    baseUrl: "https://localhost:3000/",
    parsePages: false,
    strategy: "prefix_except_default",
    defaultLocale: "en",
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: "raw_art"
    },
    pages: {
      baseUrl: {
        en: "/",
        ru: "/ru"
      },
      about: {
        en: "/about",
        ru: "/about"
      }
    }
  }
};
